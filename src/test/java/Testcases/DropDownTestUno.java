package Testcases;

import java.util.List;

import org.testng.annotations.*;

import base.TestBase;
import pages.DropdownPage;

public class DropDownTestUno extends TestBase {
	
	DropdownPage drpDown;
	
	public DropDownTestUno() {
		super();
	}
	
	@BeforeClass
	public void setUp() {
		Initialisation("headless");
		drpDown = new DropdownPage();
	}
	
	
	@Test
	public void dropDownTest() {
		drpDown.GoToPage();
		
		List<String> listItems = drpDown.getAllItems();
		
		for (int i = 0; i < listItems.size(); i ++) {
			System.out.println(listItems.get(i));
		}
		
	}
	
	@Test
	public void sayHello() {
		System.out.println("Hello");
	}
	
	@AfterClass
	public void tearDown() {
		driver.close();
	}
}
