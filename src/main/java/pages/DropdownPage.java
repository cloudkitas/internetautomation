package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import base.TestBase;

public class DropdownPage extends TestBase {
	
	
	@FindBy(linkText = "Dropdown")
	WebElement drpDownPage;
	
	@FindBy(id="dropdown")
	WebElement dropdownElement;
	
	public DropdownPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void GoToPage() {
		drpDownPage.click();
	}
	
	
	public List<String> getAllItems() {
		
		Select dropDownOptions = new Select(dropdownElement);
		List<WebElement> availOptions = dropDownOptions.getOptions();
		
		List<String> colOptions = new ArrayList<String>();
		
		for (WebElement opt : availOptions) {
			String opt1 = opt.getText();
			colOptions.add(opt1);
		}
		
		return colOptions;
		
		
		
	}
	
	
	
	

}
